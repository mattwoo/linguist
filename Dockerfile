FROM openjdk:14-jdk-alpine

RUN mkdir -p /var/www/upload/avatars
RUN chmod -R 777 /var/www/upload

ADD /target/linguist-0.1.jar /usr/src/linguist/app.jar
WORKDIR /usr/src/linguist
EXPOSE 8080
CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "app.jar"]

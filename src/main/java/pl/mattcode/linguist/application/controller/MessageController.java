package pl.mattcode.linguist.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.mattcode.linguist.infrastructure.entity.Message;
import pl.mattcode.linguist.infrastructure.entity.Namespace;
import pl.mattcode.linguist.infrastructure.entity.NamespaceRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class MessageController {

    @Autowired
    NamespaceRepository repository;

    @GetMapping("/api/namespace")
    public List<Namespace> namespaces() {
        return repository.findAll();
    }

    @GetMapping("/api/namespace/{uuid}/{lang}")
    public Namespace byLang(@PathVariable String uuid, @PathVariable String lang) {
        Namespace namespace = repository.findById(UUID.fromString(uuid)).orElse(null);
        if(null == namespace){
            return null;
        }
        List<Message> messages = namespace.getMessages().stream().filter(m -> m.getLang().equals(lang)).collect(Collectors.toList());
        namespace.setMessages(messages);
        return namespace;
    }

    @GetMapping("/api/namespace/{uuid}")
    public Namespace namespace(@PathVariable String uuid) {
        return repository.findById(UUID.fromString(uuid)).orElse(null);
    }

    @PostMapping("/api/namespace")
    public String create(@RequestBody Namespace namespace) {
        repository.save(namespace);
        return namespace.getUuid().toString();
    }

    @PostMapping("/api/namespace/{namespaceUuid}/message")
    public String createBatch(@RequestBody Message[] messages, @PathVariable String namespaceUuid) {
        Namespace namespace = repository.findById(UUID.fromString(namespaceUuid)).orElse(null);
        if (null != namespace) {
            if (messages.length == 0) {
                return "Empty messages";
            }
            namespace.clearMessages();
            repository.save(namespace);
            for (Message m : messages) {
                namespace.addMessage(m);
            }
            repository.save(namespace);
            return "OK";
        }

        return "Namespace not found";
    }

    @PutMapping("/api/namespace/{namespaceUuid}/message")
    public String addMessage(@PathVariable String namespaceUuid, @RequestBody Message message) {
        Namespace namespace = repository.findById(UUID.fromString(namespaceUuid)).orElse(null);
        if (null != namespace) {
            namespace.addMessage(message);
            repository.save(namespace);
            return "OK";
        }

        return "Namespace not found";
    }
}

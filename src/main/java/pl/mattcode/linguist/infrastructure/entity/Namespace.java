package pl.mattcode.linguist.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "namespaces")
public class Namespace {

    @Id
    @Type(type = "uuid-char")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID uuid;
    private String name;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "namespace_uuid")
    private List<Message> messages;

    public void addMessage(Message message) {
        messages.add(message);
    }

    public void removeMessage(UUID uuid) {
        messages.removeIf(e -> e.getUuid().equals(uuid));
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void clearMessages(){
        messages.clear();
    }
}

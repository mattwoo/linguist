package pl.mattcode.linguist.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "messages", uniqueConstraints = {@UniqueConstraint(columnNames = {"t_key", "lang"})})
public class Message {
    @Id
    @Type(type = "uuid-char")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID uuid;
    @Column(name = "t_key")
    private String key;
    private String lang;
    private String value;

}

package pl.mattcode.linguist.infrastructure.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface NamespaceRepository extends CrudRepository<Namespace, UUID> {

    @Override
    List<Namespace> findAll();

}
